$(document).ready(() => {
    $('#register-button').click((e) => {
        e.preventDefault();
        $('#register-button').prop('disabled', true);
        grecaptcha.ready(function () {
            grecaptcha.execute('6Ld6jWcaAAAAAArgu8eF5Dlltmc4IQkoXW7m6dJD', {
                action: 'register'
            }).then(function (token) {
                $.post('/api/auth/register', {
                    username: $('#username-input').val(),
                    email: $('#email-input').val(),
                    password: $('#password-input').val(),
                    mojang_username: $('#mojang-username-input').val(),
                    captcha: token
                }, (data) => {
                    $('#register-button').prop('disabled', false);

                    if (!data.success) {
                        displayError(data.error);
                        return;
                    }

                    note({
                        content: getTranslation(results, 'register'),
                        type: 'info',
                        time: 30
                    });
                });
            });
        });
    });

    let usernameRegex = /(?:^([a-zA-Z0-9_]{4,16})$|^([а-яА-Я0-9_]{4,16})$)/;
    let passwordRegex = /^(?=.*[A-ZА-Я])(?=.*[0-9])(?=.*[a-zа-я]).{8,}$/;
    let emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

    let lastUsernameValue = undefined;
    let lastEmailValue = undefined;
    let lastMojangUsernameValue = undefined;

    function inputNone(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputSuccess(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputError(inputId, error) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('invalid-input');

        if (error === undefined) {
            return;
        }

        $(`#${inputId}-error`).text(getTranslation(errors, error));
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
        $(`#${inputId}-error-block`).addClass('flash-error-visible');
    }

    function checkWholeValidity() {
        let enabled = $('#username-input').hasClass('valid-input') &&
            $('#email-input').hasClass('valid-input') &&
            $('#password-input').hasClass('valid-input') &&
            $('#repeat-password-input').hasClass('valid-input') &&
            !$('#mojang-username-input').hasClass('invalid-input') &&
            $('#agreed-input').is(':checked');

        $('#register-button').prop('disabled', !enabled);
    }

    $('#username-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === lastUsernameValue) {
            return;
        }
        lastUsernameValue = currentValue;

        if (currentValue === '') {
            inputNone('username');
            checkWholeValidity();
            return;
        }

        if (!usernameRegex.test(currentValue)) {
            inputError('username', 'username-format-incorrect');
            checkWholeValidity();
            return;
        }

        $.post('/api/auth/register/username_available', {
            username: currentValue
        }, (data) => {
            if (currentValue !== $(event.target).val()) {
                return;
            }

            if (!data.success) {
                displayError(data.error);
                return;
            }

            if (!data.result.available) {
                inputError('username', 'username-unavailable');
                checkWholeValidity();
                return;
            }

            inputSuccess('username');
            checkWholeValidity();
        });
    });

    $('#email-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === lastEmailValue) {
            return;
        }
        lastEmailValue = currentValue;

        if (currentValue === '') {
            inputNone('email');
            checkWholeValidity();
            return;
        }

        if (!emailRegex.test(currentValue)) {
            inputError('email', 'email-format-incorrect');
            checkWholeValidity();
            return;
        }

        $.post('/api/auth/register/email_available', {
            email: currentValue
        }, (data) => {
            if (currentValue !== $(event.target).val()) {
                return;
            }

            if (!data.success) {
                displayError(data.error);
                return;
            }

            if (!data.result.available) {
                inputError('email', 'email-unavailable');
                checkWholeValidity();
                return;
            }

            inputSuccess('email');
            checkWholeValidity();
        });
    });

    $('#password-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('password');
            checkWholeValidity();
            return;
        }

        if (!passwordRegex.test(currentValue)) {
            inputError('password', 'password-format-incorrect');
            checkWholeValidity();
            return;
        }

        inputSuccess('password');
        $('#repeat-password-input').trigger('change');
        checkWholeValidity();
    });

    $('#repeat-password-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('repeat-password');
            checkWholeValidity();
            return;
        }

        if (!passwordRegex.test(currentValue)) {
            inputError('repeat-password', 'password-format-incorrect');
            checkWholeValidity();
            return;
        }

        if (currentValue !== $('#password-input').val()) {
            inputError('repeat-password', 'repeat-password-incorrect');
            checkWholeValidity();
            return;
        }

        inputSuccess('repeat-password');
        checkWholeValidity();
    });

    $('#mojang-username-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === lastMojangUsernameValue) {
            return;
        }
        lastMojangUsernameValue = currentValue;

        if (currentValue === '') {
            inputNone('mojang-username');
            checkWholeValidity();
            return;
        }

        $.post('/api/auth/register/mojang_username_exists', {
            mojang_username: currentValue
        }, (data) => {
            if (currentValue !== $(event.target).val()) {
                return;
            }

            if (!data.success) {
                displayError(data.error);
                return;
            }

            if (!data.result.exists) {
                inputError('mojang-username', 'mojang-username-does-not-exist');
                checkWholeValidity();
                return;
            }

            inputSuccess('mojang-username');
            checkWholeValidity();
        });
    });

    $('#agreed-input').on('change', () => {
        checkWholeValidity();
    });
});

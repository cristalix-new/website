// Блок система оплаты

$('#payment-button').click(function() {
    var element = $('#modal-frame');
    var div = document.createElement('div');
    document.getElementById("modal-frame").classList.remove('re-frame2');
    document.getElementById("modal-frame").classList.add('re-frame1');
    $('body').addClass('stop-scrolling')
    document.getElementById("payment-system").classList.remove('vanish');
    document.getElementById("payment-system").classList.add('display');

    $(document).on('click', '#donate-exit-btn', function() {
        document.getElementById("modal-frame").classList.remove('re-frame1');
        document.getElementById("modal-frame").classList.add('re-frame2');
        $('body').removeClass('stop-scrolling')
        document.getElementById("payment-system").classList.remove('display');
        document.getElementById("payment-system").classList.add('vanish');
    });
});

// Cпособы перевода

$('#chek-ps-fk').click(function() {
    var element = $('#payment-system');
    var div = document.createElement('div');

    document.getElementById("operators-Interkassa").classList.add('block-operators-vanish');
    document.getElementById("operators-enot").classList.add('block-operators-vanish');
    document.getElementById("operators-unitpay").classList.add('block-operators-vanish');
    document.getElementById("operators-freekassa").classList.remove('block-operators-vanish');

});
$(document).on('click', '#chek-ps-ik', function() {
    document.getElementById("operators-enot").classList.add('block-operators-vanish');
    document.getElementById("operators-freekassa").classList.add('block-operators-vanish');
    document.getElementById("operators-unitpay").classList.add('block-operators-vanish');
    document.getElementById("operators-Interkassa").classList.remove('block-operators-vanish');
});
$(document).on('click', '#chek-ps-et', function() {
    document.getElementById("operators-freekassa").classList.add('block-operators-vanish');
    document.getElementById("operators-Interkassa").classList.add('block-operators-vanish');
    document.getElementById("operators-unitpay").classList.add('block-operators-vanish');
    document.getElementById("operators-enot").classList.remove('block-operators-vanish');
});
$(document).on('click', '#chek-ps-up', function() {
    document.getElementById("operators-freekassa").classList.add('block-operators-vanish');
    document.getElementById("operators-Interkassa").classList.add('block-operators-vanish');
    document.getElementById("operators-enot").classList.add('block-operators-vanish');
    document.getElementById("operators-unitpay").classList.remove('block-operators-vanish');
});

$('#continue-btn').click(function() {
    if (!document.getElementById('agreed-donate-input').checked) {
        displayError('Для того, чтобы продолжить, примите условия соглашений');
        return;
    }
    if (document.getElementById('username-input').value.length == 0) {
        displayError('Для того, чтобы продолжить, введите никнейм');
        return;
    }
    if (document.getElementById('coins-input').value.length == 0) {
        displayError('Для того, чтобы продолжить, введите сумму в кристаллах');
        return;
    }
    var payVia = document.getElementById('chek-ps-ik').checked ? 'ik' : document.getElementById('chek-ps-fk').checked ? 'fk' : 'enot';
    window.open('https://cristalix.ru/account/user_donate?user=' + document.getElementById('username-input').value + '&sum=' + document.getElementById('coins-input').value + '&payVia=' + payVia, '_blank')
})
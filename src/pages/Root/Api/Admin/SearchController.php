<?php

namespace Pages\Root\Api\Admin;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\PermissionsExtension;
use Cristalix\Engine\Extensions\RecaptchaExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\OTPExtension;
use Cristalix\Model\User;

class SearchController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use RecaptchaExtension;
    use OTPExtension;
    use PermissionsExtension;
    use SessionExtension;

    const PERMISSION = 'site.admin.search';

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        //$this->initializePermissions($config['permissions']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['search_text'])) {
            return;
        }

        if ($this->getUser() == null 
        //|| !in_array(self::PERMISSION, $this->getPermissions($this->getUser()))
        ) {
            $this->error('unauthorized');
            return;
        }

        $search_text = $context->getRequest()->post('search_text');

        $search_results = $this->getDatabase()->queryData("SELECT users.id FROM users
                            WHERE users.username = :search_text ", [
            ':search_text' => $search_text
        ]);
        if (empty($search_results)) {
            $this->error('nothing-found');
            return;
        }

        $user = User::fetch($this->getDatabase(), $search_results[0]->id);

        $this->renderPage($context->getTwig(), 'static/admin/account.html', [
            'cookie_accept' => $context->getRequest()->getCookie('cookie_accept'),
            'theme' => $context->getRequest()->getCookie('theme'),
            'id' => $user->getId(),
            'search_text' => $search_text,
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'staff_group' => $user->getStaffGroup(),
            'donate_group' => $user->getDonateGroup(),
            'money' => $user->getGold(),
            'gold' => $user->getBonuses(),
            'score' => $user->getExperience(),
            'total_gold' => $user->getTotalGold()
        ]);
        return;

    }
}
<?php /** @noinspection DuplicatedCode */

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\MinioExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\PermissionsExtension;
use Exception;

class CapeController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use PermissionsExtension;
    use SessionExtension;
    use MinioExtension;

    const PERMISSION = 'site.cape';

    private array $all_sizes;
    private array $standard_sizes;
    private array $size_relation;
    private array $resize_relation;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializePermissions($config['groups']);
        $this->initializeMinio($config['profile_image_storage']);

        $this->all_sizes = $config['capes']['all_sizes'];
        $this->standard_sizes = $config['capes']['standard_sizes'];
        $this->size_relation = $config['capes']['size_relation'];
        $this->resize_relation = $config['capes']['resize_relation'];
    }

    public function processRequest(RequestContext $context): void
    {
        if ($context->getRequest()->file('cape') == null) {
            $this->error('no-cape-file');
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        if (!in_array(self::PERMISSION, $this->getPermissions($this->getUser()))) {
            $this->error('no-permissions');
            return;
        }

        $cape = $context->getRequest()->file('cape');

        if ($cape['size'] > 500 * 1024) {
            $this->error('file-is-too-big');
            return;
        }

        if (strtolower(pathinfo(basename($cape['name']), PATHINFO_EXTENSION)) != 'png' ||
            mime_content_type($cape['tmp_name']) != 'image/png') {
            $this->error('file-is-not-png');
            return;
        }

        $size = getimagesize($cape['tmp_name']);
        $size_string = $size[0] . 'x' . $size[1];

        if (!in_array($this->resize_relation[$size_string], $this->all_sizes)) {
            $this->error('incorrect-size');
            return;
        }

        if (!$this->getUser()->isHdActive() && !in_array($this->resize_relation[$size_string], $this->standard_sizes)) {
            $this->error('cape-buy-hd-size');
            return;
        }

        $real_size = explode('x', $this->resize_relation[$size_string]);
        $cape_image = imagecreatetruecolor($real_size[0], $real_size[1]);
        $cape_source_image = imagecreatefrompng($cape['tmp_name']);
        imagecopymerge($cape_image, $cape_source_image, 0, 0, 0, 0, $size[0], $size[1], 100);
        imagedestroy($cape_source_image);

        $cape_path = tempnam(sys_get_temp_dir(), "cape");
        imageAlphaBlending($cape_image, true);
        imageSaveAlpha($cape_image, true);
        imagepng($cape_image, $cape_path, 8);
        imagedestroy($cape_image);

        try {
            $this->uploadFile($cape_path, 'cape/' . $this->getUser()->getUuid());
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        } finally {
            unlink($cape_path);
        }

        $this->result([]);
    }
}
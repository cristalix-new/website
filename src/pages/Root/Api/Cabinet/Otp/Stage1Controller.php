<?php

namespace Pages\Root\Api\Cabinet\Otp;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\OTPExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Base2n;
use Exception;

class Stage1Controller extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use SessionExtension;
    use OTPExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeOTP($config['otp_salt']);
    }

    public function processRequest(RequestContext $context): void
    {
        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        if ($this->getUser()->isOtpEnabled()) {
            $this->error('otp-already-enabled');
            return;
        }

        $base32 = new Base2n(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', FALSE, TRUE, TRUE);
        try {
            $secret = $base32->encode(random_bytes(10));
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }

        $this->getDatabase()->query("INSERT INTO ga_secrets (user_id, secret) 
                                        VALUES (:user_id, :secret) ON CONFLICT(user_id) DO UPDATE SET secret = :secret", [
            ':user_id' => $this->getUser()->getId(),
            ':secret' => $secret
        ]);

        $this->result([
            'image_url' => $this->generateOTPImage($this->getUser()->getUsername(), $this->getUser()->getId(), $secret)
        ]);
    }
}
<?php /** @noinspection DuplicatedCode */

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\AvailabilityVerificationExtension;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\DonateExtension;
use Cristalix\Engine\Extensions\MinioExtension;
use Cristalix\Engine\Extensions\PasswordVerificationExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\FormatVerificationExtension;
use Cristalix\Model\User;

class SkinController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use SessionExtension;
    use MinioExtension;

    private array $all_sizes;
    private array $standard_sizes;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeMinio($config['profile_image_storage']);

        $this->all_sizes = $config['skins']['all_sizes'];
        $this->standard_sizes = $config['skins']['standard_sizes'];
    }

    public function processRequest(RequestContext $context): void
    {
        if ($context->getRequest()->file('skin') == null) {
            $this->error('no-skin-file');
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        $skin = $context->getRequest()->file('skin');

        if ($skin['size'] > 500 * 1024) {
            $this->error('file-is-too-big');
            return;
        }

        if (mime_content_type($skin['tmp_name']) != 'image/png') {
            $this->error('file-is-not-png');
            return;
        }

        $size = getimagesize($skin['tmp_name']);
        $size_string = $size[0] . 'x' . $size[1];

        if (!in_array($size_string, $this->all_sizes)) {
            $this->error('incorrect-size');
            return;
        }

        if (!$this->getUser()->isHdActive() && !in_array($size_string, $this->standard_sizes)) {
            $this->error('skin-buy-hd-size');
            return;
        }

        $skin_image = imagecreatefrompng($skin['tmp_name']);

        $skin_path = tempnam(sys_get_temp_dir(), "cape");
        imageAlphaBlending($skin_image, true);
        imageSaveAlpha($skin_image, true);
        imagepng($skin_image, $skin_path, 8);
        imagedestroy($skin_image);

        try {
            $this->uploadFile($skin_path, 'skin/' . $this->getUser()->getUuid());
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        } finally {
            unlink($skin_path);
        }

        $this->result([]);
    }
}
<?php

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Model\User;

class HotDataController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;

    public function initialize(array $config): void
    {
        $this->initializeDatabase($config['database']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['username'])) {
            return;
        }

        $username = $context->getRequest()->post('username');

        $search_results = $this->getDatabase()->queryData("SELECT users.id FROM users WHERE LOWER(username) = LOWER(:name)", [
            ':name' => $username
        ]);

        if (empty($search_results)) {
            $this->error('nothing-found');
            return;
        }

        $user = User::fetch($this->getDatabase(), $search_results[0]->id);

        $donate_groups = [
            'NO' => [
                'price' => 0,
                'name' => 'Без доната',
                'color' => '',
                'plus' => false
            ],
            'IRON' => [
                'price' => 150,
                'name' => 'Iron',
                'color' => 'cccccc',
                'plus' => false
            ],
            'VIP' => [
                'price' => 400,
                'name' => 'Vip',
                'color' => 'ffff66',
                'plus' => false
            ],
            'VIP+' => [
                'price' => 900,
                'name' => 'Vip',
                'color' => 'ffff66',
                'plus' => true
            ],
            'GOLD' => [
                'price' => 1200,
                'name' => 'Gold',
                'color' => 'ffff66',
                'plus' => false
            ],
            'PREMIUM' => [
                'price' => 1800,
                'name' => 'Premium',
                'color' => '70f564',
                'plus' => false
            ],
            'PREMIUM+' => [
                'price' => 3000,
                'name' => 'Premium',
                'color' => '70f564',
                'plus' => true
            ],
            'DIAMOND' => [
                'price' => 6000,
                'name' => 'Diamond',
                'color' => '75c1ff',
                'plus' => false
            ],
            'MVP' => [
                'price' => 12000,
                'name' => 'MVP',
                'color' => '75c1ff',
                'plus' => false
            ],
            'MVP+' => [
                'price' => 18000,
                'name' => 'MVP',
                'color' => '75c1ff',
                'plus' => true
            ],
            'EMERALD' => [
                'price' => 25000,
                'name' => 'Emerald',
                'color' => '009900',
                'plus' => false
            ],
            'SPONSOR' => [
                'price' => 30000,
                'name' => 'Sponsor',
                'color' => 'ffa000',
                'plus' => false
            ],
            'HE_TOPT' => [
                'price' => 50000,
                'name' => 'НЕ ТОРТ',
                'color' => 'ff9900',
                'plus' => false
            ],
            'GOD' => [
                'price' => 100000,
                'name' => 'God',
                'color' => 'fa9a00',
                'plus' => false
            ],
        ];
        $staff_groups = [
            [
                'name' => 'Игрок',
                'color' => ''
            ],
            [
                'name' => 'Тестер',
                'color' => ''
            ],
            [
                'name' => 'YouTube',
                'color' => 'ff3333'
            ],
            [
                'name' => 'Helper',
                'color' => '75c1ff'
            ],
            [
                'name' => 'Moderator',
                'color' => 'ff4dff'
            ],
            [
                'name' => 'Sr. Moderator',
                'color' => 'e600e6'
            ],
            [
                'name' => 'Curator',
                'color' => 'ff3333'
            ],
            [
                'name' => 'Admin',
                'color' => 'ff3333'
            ],
            [
                'name' => 'Developer',
                'color' => '75c1ff'
            ],
            [
                'name' => 'Owner',
                'color' => 'ff3333'
            ],
            [
                'name' => 'Cur. Builder',
                'color' => '7fffd4'
            ],
            [
                'name' => 'Builder',
                'color' => '7fffd4'
            ],
            [
                'name' => 'Sr. Builder',
                'color' => '7fffd4'
            ],
        ];
        $total_gold = $user->getTotalGold();
        $current_group = '';
        $next_group = '';
        $current_sum = 0;
        $percent = 1.0;
        $next_gold_group = 100000;
        $user_groups = [];
        $flag = false;

        foreach ($donate_groups as $group_name => $data) {
            $sum = $data['price'];
            if ($total_gold >= $sum) {
                $user_groups[] = $group_name;
                $current_sum = $sum;
                $current_group = $group_name;
                $next_group = $group_name;
                $flag = true;
            } else if ($flag) {
                $next_group = $group_name;
                $percent = ($total_gold - $current_sum) / ($sum - $current_sum);
                $next_gold_group = $sum;
                break;
            }
        }

        $current_group_data = $donate_groups[$current_group];
        $current_staff_group_data = $staff_groups[$user->getStaffGroup()];

        $this->result([
            'donate_group' => $user->getDonateGroup(),
            'staff_group' => $user->getStaffGroup(),
            'display_donate_group' => $current_group_data,
            'display_staff_group' => $current_staff_group_data,
            'total_gold' => $user->getTotalGold(),
            'cur_group' => $current_group,
            'next_group' => $next_group,
            'next_group_name' => $donate_groups[$next_group]['name'] . ($donate_groups[$next_group]['plus'] ? '+' : ''),
            'next_gold_group' => $next_gold_group,
            'percent' => $percent,
            'groups' => $user_groups
        ]);
    }
}
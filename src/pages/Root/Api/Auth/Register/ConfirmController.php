<?php

namespace Pages\Root\Api\Auth\Register;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\AvailabilityVerificationExtension;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\MinioExtension;
use Cristalix\Engine\Extensions\PasswordVerificationExtension;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\XenforoApiExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Model\User;
use Exception;

class ConfirmController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use PasswordVerificationExtension;
    use SessionExtension;
    use AvailabilityVerificationExtension;
    use XenforoApiExtension;
    use MinioExtension;

    private string $default_skin_path;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializePasswords($config['password_salt']);
        $this->initializeXenforoApi($config['forum_api']);
        $this->initializeMinio($config['profile_image_storage']);

        $this->default_skin_path = $config['skins']['default'];
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['key'])) {
            return;
        }

        $key = $context->getRequest()->post('key');

        $registration_data = $this->getDatabase()->queryData("SELECT * FROM registrations WHERE \"key\" = :key AND expires > to_timestamp(:time)", [
            ':key' => $key,
            ':time' => time()
        ]);

        if (empty($registration_data)) {
            $this->error('wrong-key');
            return;
        }

        $data = $registration_data[0];

        /*
        try {
            $forum_user = $this->createForumUser($data->username, $data->email)->user;
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }
        */

        $this->getDatabase()->query("DELETE FROM registrations WHERE \"key\" = :key", [
            ':key' => $key
        ]);

        $user_id = $this->getDatabase()->queryData("INSERT INTO users (username, forum_user_id) 
                                            VALUES (:username, :forum_user_id) RETURNING id", [
            ':username' => $data->username,
            // ':gender' => $data->gender,
            // ':forum_user_id' => $forum_user->user_id
            ':forum_user_id' => 543
        ])[0]->id;

        $this->getDatabase()->query("INSERT INTO credentials (user_id, email, password_hash) VALUES (:user_id, :email, :password_hash)", [
            ':user_id' => $user_id,
            ':email' => $data->email,
            ':password_hash' => $this->getPasswordHash($user_id, $data->password)
        ]);

        $this->getDatabase()->query("INSERT INTO balances (user_id) VALUES (:user_id)", [
            ':user_id' => $user_id
        ]);

        $this->getDatabase()->query("INSERT INTO hd_subscriptions (user_id) VALUES (:user_id)", [
            ':user_id' => $user_id
        ]);

        $this->setUser(User::fetch($this->getDatabase(), $user_id));

        if ($data->mojang_username != null) {
            $skin_temp_name = null;
            try {
                $skin_url = $this->getMojangSkinURL($data->mojang_username);
                $skin_temp_name = tempnam(sys_get_temp_dir(), "mojang_skin");
                file_put_contents($skin_temp_name, file_get_contents($skin_url));

                $skin_image = imagecreatefrompng($skin_temp_name);
                imageAlphaBlending($skin_image, true);
                imageSaveAlpha($skin_image, true);
                imagepng($skin_image, $skin_temp_name, 8);
                imagedestroy($skin_image);

                try {
                    $this->uploadFile($skin_temp_name, 'skin/' . $this->getUser()->getUuid());
                } catch (Exception $e) {
                    error_log($e);
                }
            } catch (Exception $e) {
                error_log($e);
            } finally {
                if ($skin_temp_name != null) {
                    unlink($skin_temp_name);
                }
            }
        }



        $this->result([]);
    }

    private function getMojangSkinURL(string $username): ?string
    {
        $id = json_decode(file_get_contents("https://api.mojang.com/users/profiles/minecraft/$username"))->id;
        if ($id == null) {
            return null;
        }
        $profile_data = json_decode(file_get_contents(
            "https://sessionserver.mojang.com/session/minecraft/profile/$id"));
        $textures = null;
        foreach ($profile_data->properties as $property) {
            if ($property->name == "textures") {
                $textures = $property->value;
            }
        }
        if ($textures == null) {
            return null;
        }
        return json_decode(base64_decode($textures))->textures->SKIN->url;
    }
}
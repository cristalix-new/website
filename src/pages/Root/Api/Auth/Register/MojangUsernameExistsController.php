<?php

namespace Pages\Root\Api\Auth\Register;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\AvailabilityVerificationExtension;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\RequestContext;

class MojangUsernameExistsController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use AvailabilityVerificationExtension;

    public function initialize(array $config): void
    {
        $this->initializeDatabase($config['database']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['mojang_username'])) {
            return;
        }

        $username = $context->getRequest()->post('mojang_username');

        $this->result([
            'exists' => $this->doesMojangUsernameExist($username)
        ]);
    }


}
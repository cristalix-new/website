<?php

namespace Pages\Root\Api\Auth\Register;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\AvailabilityVerificationExtension;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\RequestContext;

class EmailAvailableController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use AvailabilityVerificationExtension;

    public function initialize(array $config): void
    {
        $this->initializeDatabase($config['database']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['email'])) {
            return;
        }

        $email = $context->getRequest()->post('email');

        $this->result([
            'available' => $this->isEmailAvailable($email)
        ]);
    }
}
<?php

$config = [
    'database' => [
        'hostname' => 'postgres.common-services',
        'username' => 'cristalix',
        'password' => 'cristalix',
        'database' => 'cristalix'
    ],

    'hd_options' => [
        'week' => [
            'price' => 75,
            'name' => 'Неделя',
            'interval' => '7 DAYS',
            'value' => 'week'
        ],
        'month' => [
            'price' => 250,
            'name' => 'Месяц',
            'interval' => '30 DAYS',
            'value' => 'month'
        ],
        '3month' => [
            'price' => 600,
            'name' => '3 месяца',
            'interval' => '90 DAYS',
            'value' => '3month'
        ],
        '6month' => [
            'price' => 1000,
            'name' => '6 месяцев',
            'interval' => '180 DAYS',
            'value' => '6month'
        ],
        '9month' => [
            'price' => 1500,
            'name' => '9 месяцев',
            'interval' => '270 DAYS',
            'value' => '9month'
        ],
        'year' => [
            'price' => 1800,
            'name' => 'Год',
            'interval' => '365 DAYS',
            'value' => 'year'
        ],
    ],

    'sessions_redis' => 'tcp://redis.common-services:6379?prefix=CRISTALIX_WEB_SESSION_',

    'redis' => [
        'hostname' => 'redis.common-services',
        'port' => 6379,
        'password' => null
    ],

    'servers' => [
        'minigames' => [
            'name' => 'MiniGames',
            'ip' => ''
        ],
        'neotech' => [
            'name' => 'NeoTech',
            'ip' => ''
        ],
        'magica' => [
            'name' => 'Magica',
            'ip' => ''
        ],
        'technomagic' => [
            'name' => 'TechnoMagic',
            'ip' => ''
        ],
        'divinepvp' => [
            'name' => 'DivinePVP',
            'ip' => ''
        ],
        'skyvoid' => [
            'name' => 'SkyVoid',
            'ip' => ''
        ],
        'galax' => [
            'name' => 'Galax',
            'ip' => ''
        ],
        'rpgparfeon' => [
            'name' => 'RPG Parfeon',
            'ip' => ''
        ],
        'pixelmon' => [
            'name' => 'Pixelmon',
            'ip' => ''
        ]
    ],

    'recaptcha_key' => '6Ld6jWcaAAAAAFXWm3L76yiSb4tvusRTaOIPe1iY',

    'password_salt' => 'BNGPSW',

    'otp_salt' => 'hahabenis',

    'tower_rest_api_url' => 'http://tower-rest-service.common-services',

    'prices' => [
        'username_change' => 999
    ],

    'profile_image_storage' => [
        'endpoint' => 'https://storage.c7x.dev',
        'access_key' => 'c293cdc4-5e9d-4ad9-864b-29d40b53f245',
        'secret_key' => '0286d5c6-608c-4ac1-a393-5172eec48ed0',
        'bucket' => 'profile-images'
    ],

    'skins' => [
        'all_sizes' => ['64x32', '64x64', '128x64', '256x128', '512x256', '1024x512', '2048x1024', '4096x2048'],
        'standard_sizes' => ['64x32', '64x64'],

        'default' => __DIR__ . '/data/default-skin.png'
    ],

    'capes' => [
        'all_sizes' => ['22x17', '44x34', '88x68', '176x136', '352x272', '704x544', '1408x1088'],
        'standard_sizes' => ['22x17'],

        'size_relation' => [
            '22x17' => '64x32',
            '44x34' => '128x64',
            '88x68' => '256x128',
            '176x136' => '512x256',
            '352x272' => '1024x512',
            '704x544' => '2048x1024',
            '1408x1088' => '4096x2048'
        ],
        'resize_relation' => [
            '64x32' => '22x17',
            '128x64' => '44x34',
            '256x128' => '88x68',
            '512x256' => '176x136',
            '1024x512' => '352x272',
            '2048x1024' => '704x544',
            '4096x2048' => '1408x1088'
        ]
    ],

    'mail' => [
        'host' => 'smtp.beget.com',
        'username' => 'cristalix@ilyafx.ru',
        'password' => 'Vp*ALx3i',
        'secure' => true,
        'port' => 25,

        'from' => 'cristalix@ilyafx.ru'
    ],

    'forum_api' => [
        'url' => 'https://forum-new-cristalix.1488.me/api/',
        'key' => 'eNEWwei5wn_OMFTGNP4hhYt-XQUo1FjL'
    ],

    'groups' => [
        'permissions_file' => __DIR__ . '/groups.csv',
        'staff' => [
            0 => [
                'name' => 'PLAYER'
            ]
        ],
        'donate' => [
            0 => [
                'name' => 'NO',
                'price' => 0
            ]
        ]
    ]
];

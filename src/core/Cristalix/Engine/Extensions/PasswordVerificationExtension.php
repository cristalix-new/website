<?php


namespace Cristalix\Engine\Extensions;


trait PasswordVerificationExtension
{
    private string $password_salt;

    protected function initializePasswords(string $salt): void
    {
        $this->password_salt = $salt;
    }

    protected function getPasswordHash(string $user_id, string $password): string
    {
        return sha1($user_id . $this->password_salt . $password);
    }

    protected function verifyPassword(string $user_id, string $password_hash, string $password): bool
    {
        return $password_hash == $this->getPasswordHash($user_id, $password);
    }
}
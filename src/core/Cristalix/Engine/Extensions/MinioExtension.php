<?php


namespace Cristalix\Engine\Extensions;

use Aws\S3\S3Client;
use RuntimeException;

trait MinioExtension
{
    private S3Client $minio_client;
    private string $bucket;

    protected function initializeMinio(array $minio_config): void
    {
        $this->minio_client = new S3Client([
            'version' => 'latest',
            'region' => '',
            'endpoint' => $minio_config['endpoint'],
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key' => $minio_config['access_key'],
                'secret' => $minio_config['secret_key']
            ]
        ]);
        $this->bucket = $minio_config['bucket'];
    }

    protected function uploadFile(string $local_path, string $path): void
    {
        if (!file_exists($local_path)) {
            throw new RuntimeException('File ' . $local_path . ' does not exist');
        }
        $this->minio_client->upload($this->bucket, $path, file_get_contents($local_path));
    }
}
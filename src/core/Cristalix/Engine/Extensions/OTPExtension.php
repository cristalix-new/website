<?php


namespace Cristalix\Engine\Extensions;


use Sonata\GoogleAuthenticator\GoogleAuthenticator;
use Sonata\GoogleAuthenticator\GoogleQrUrl;

trait OTPExtension
{
    private string $otp_salt;

    protected function initializeOTP(string $salt): void {
        $this->otp_salt = $salt;
    }

    protected function generateOTPImage(string $account_name, string $user_id, string $secret): string {
        return GoogleQrUrl::generate($account_name, $secret, 'Cristalix');
    }

    protected function verifyOTP(string $user_id, string $secret, string $code): bool {
        return (new GoogleAuthenticator())->checkCode($secret, $code);
    }
}
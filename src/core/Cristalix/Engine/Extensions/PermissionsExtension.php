<?php


namespace Cristalix\Engine\Extensions;

use Cristalix\Model\User;

trait PermissionsExtension
{
    private array $groups;

    protected function initializePermissions(array $groups): void
    {
        // TODO
        $this->groups = $groups;
    }

    public function getPermissions(User $user): array
    {
        $arr = [];
        if ($user->getDonateGroup() >= 5) {
            $arr[] = 'site.cape';
        }
        if ($user->getDonateGroup() >= 8) {
            $arr[] = 'site.namecolor';
        }
        return $arr;
    }
}
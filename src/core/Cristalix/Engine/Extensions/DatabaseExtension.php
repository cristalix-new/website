<?php

namespace Cristalix\Engine\Extensions;

use Cristalix\Engine\Database;

trait DatabaseExtension
{
    private bool $database_ready = false;
    private array $database_config;
    private Database $database;

    public function getDatabase(): Database
    {
        if (!$this->database_ready) {
            $this->database = new Database($this->database_config);
            $this->database_ready = true;
        }
        return $this->database;
    }

    protected function initializeDatabase(array $config): void
    {
        $this->database_config = $config;
    }
}
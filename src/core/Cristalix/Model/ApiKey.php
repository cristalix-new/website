<?php


namespace Cristalix\Model;


use Cristalix\Engine\Database;

class ApiKey
{
    private int $id;
    private string $secret;
    private array $permissions;

    public static function fetch(Database $database, int $id): ?ApiKey
    {
        $key_result = $database->queryData("SELECT access_key FROM api_key WHERE id = :id", [
            ':id' => $id
        ]);

        if (empty($key_result)) {
            return null;
        }

        $key_result = $key_result[0];

        $permissions_result = $database->queryData("SELECT access_key FROM api_permissions WHERE id = :id", [
            ':id' => $id
        ]);

        if (empty($permissions_result)) {
            return null;
        }


        return new ApiKey($id, $key_result, $permissions_result);
    }

    public function __construct(int $id, string $key_result, array $permissions_result)
    {
        $this->id = $id;
        $this->secret = $key_result;
        $this->permissions = $permissions_result;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function getPermissions(): array
    {
        return $this->permissions;
    }

    public function getData(): array
    {
        return [
            'id' => $this->id,
            'secret' => $this->secret,
            'permissions' => $this->permissions
        ];
    }

}


?>
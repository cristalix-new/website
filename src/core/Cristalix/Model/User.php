<?php


namespace Cristalix\Model;


use Cristalix\Engine\Database;

class User
{
    private int $id;
    private int $forum_user_id;
    private string $uuid;
    private string $username;
    private string $email;
    private int $donate_group;
    private int $staff_group;
    private int $color;
    private int $gold;
    private int $bonuses;
    private int $experience;
    private int $total_gold;
    private string $hd_expires;
    private bool $otp_enabled;
    private bool $hd_active;
    private bool $skin_model;

    public static function fetch(Database $database, int $id): ?User
    {
        $user_results = $database->queryData("SELECT users.id, 
                            users.forum_user_id, users.uuid, users.username, credentials.email, 
                            users.donate_group, users.staff_group, users.color, users.skin_model,
                            b.gold, b.bonuses, b.experience, b.total_gold, 
                            credentials.ga_secret, expires as hd_expires,
                            (CASE WHEN hs.expires > to_timestamp(:time) THEN 1 ELSE 0 END) AS hd_active
                            FROM users 
                            LEFT JOIN balances b on users.id = b.user_id     
                            LEFT JOIN credentials on users.id = credentials.user_id
                            LEFT JOIN hd_subscriptions hs on users.id = hs.user_id WHERE id = :id", [
            ':id' => $id,
            ':time' => time()
        ]);

        if (empty($user_results)) {
            return null;
        }

        $user_result = $user_results[0];

        return new User(intval($user_result->id), intval($user_result->forum_user_id), $user_result->username,
            $user_result->uuid, $user_result->email, intval($user_result->donate_group),
            intval($user_result->staff_group), intval($user_result->color), intval($user_result->gold),
            intval($user_result->bonuses), intval($user_result->experience), intval($user_result->total_gold),
            $user_result->ga_secret != null, $user_result->hd_active == 1, $user_result->skin_model, $user_result->hd_expires);
    }

    public function __construct(int $id, int $forum_user_id, string $username, string $uuid, string $email, int $donate_group, int $staff_group,
                                int $color, int $gold, int $bonuses, int $experience, int $total_gold, bool $otp_enabled, bool $hd_active, bool $skin_model, string $hd_expires)
    {
        $this->id = $id;
        $this->forum_user_id = $forum_user_id;
        $this->username = $username;
        $this->uuid = $uuid;
        $this->email = $email;
        $this->donate_group = $donate_group;
        $this->staff_group = $staff_group;
        $this->color = $color;
        $this->gold = $gold;
        $this->bonuses = $bonuses;
        $this->experience = $experience;
        $this->total_gold = $total_gold;
        $this->otp_enabled = $otp_enabled;
        $this->hd_active = $hd_active;
        $this->skin_model = $skin_model;
        $this->hd_expires = $hd_expires;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getForumUserId(): int
    {
        return $this->forum_user_id;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDonateGroup(): int
    {
        return $this->donate_group;
    }

    public function getStaffGroup(): int
    {
        return $this->staff_group;
    }

    public function getColor(): int
    {
        return $this->color;
    }

    public function getGold(): int
    {
        return $this->gold;
    }

    public function getBonuses(): int
    {
        return $this->bonuses;
    }

    public function getExperience(): int
    {
        return $this->experience;
    }

    public function getTotalGold(): int
    {
        return $this->total_gold;
    }

    public function isOtpEnabled(): bool
    {
        return $this->otp_enabled;
    }

    public function isHdActive(): bool
    {
        return $this->hd_active;
    }

    public function getSkinModel(): bool
    {
        return $this->skin_model;
    }

    public function getHdExpires(): string
    {
        return $this->hd_expires;
    }

    public function getData(): array
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'username' => $this->username,
            'email' => $this->email,
            'donate_group' => $this->donate_group,
            'staff_group' => $this->staff_group,
            'color' => $this->color,
            'gold' => $this->gold,
            'bonuses' => $this->bonuses,
            'experience' => $this->experience,
            'total_gold' => $this->total_gold,
            'otp_enabled' => $this->otp_enabled,
            'hd_active' => $this->hd_active,
            'skin_model' => $this->skin_model,
            'hd_expires' => $this->hd_expires,
            'forum_hash' => hash('sha512', $this->username . $this->email . '54VcaHt4!@3gVawsHbvbCVae3`123CBNbCVe412CASvHY%te545EvZXCWSnN%$32T6876gFDF'),
            'hex_color' => '#' . substr('000000' . dechex($this->color),-6)
        ];
    }
}